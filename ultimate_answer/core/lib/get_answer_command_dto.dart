class GetAnswerCommandDTO {
  final String question;

  GetAnswerCommandDTO(this.question);
}
